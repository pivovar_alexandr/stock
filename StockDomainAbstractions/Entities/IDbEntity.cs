﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StockDomainAbstractions.Entities
{
    public interface IDbEntity
    {
        [Key]
        Guid Id { get; set; }
    }
}
