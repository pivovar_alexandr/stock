﻿using StockDomainAbstractions.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StockEntities.Code
{
    [Table("Categorys")]
    public class Category : DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        /// <summary>
        /// Название категории
        /// </summary>
        [StringLength(32)]
        public string NameCategory { get; set; }
        /// <summary>
        /// Отношение один ко многим
        /// </summary>
        public virtual List<Product> Products { get; set; }
        /// <summary>
        /// Отношение многие к одному
        /// </summary>
        public virtual List<Division> Divisions { get; set; }
    }
}
