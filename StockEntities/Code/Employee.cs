﻿using StockDomainAbstractions.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StockEntities.Code
{
    [Table("Employees")]
    public class Employee : DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        /// <summary>
        /// ФИО
        /// </summary>
        [StringLength(32)]
        public string Name { get; set; }
        /// <summary>
        /// Хранит Id Логин и пароль
        /// Отношение один к одному
        /// </summary>
        public virtual LogPass LogPasss { get; set; }
        /// <summary>
        /// Хранит Id отдела
        /// Отношение многие к одному
        /// </summary>
        public virtual Division Divisions { get; set; }
        /// <summary>
        /// Хранит Id профиля
        /// Отношение один к одному
        /// </summary>
        public virtual EmployeeProfile EmployeeProfiles { get; set; }
    }
}
