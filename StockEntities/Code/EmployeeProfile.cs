﻿using System;
using StockDomainAbstractions.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StockEntities.Code
{
    [Table("EmployeeProfiles")]
    public class EmployeeProfile : DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        /// <summary>
        /// Возраст сотрудника
        /// </summary>
        public int Age { get; set; }
        /// <summary>
        /// Зарплата
        /// </summary>
        public double Salary { get; set; }
        /// <summary>
        /// Адрес прожвания
        /// </summary>
        [StringLength(11)]
        public string Adress { get; set; }
        /// <summary>
        /// Номер телефона
        /// </summary>
        [StringLength(11)]
        public string NumberPhone { get; set; }
    }
}
