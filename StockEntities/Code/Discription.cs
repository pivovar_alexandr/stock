﻿using System;
using StockDomainAbstractions.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StockEntities.Code
{
    [Table("Discriptions")]
    public class Discription : DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        /// <summary>
        /// Количество товара
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// Описание товара
        /// </summary>
        [StringLength(8)]
        public string Dimension { get; set; }
        /// <summary>
        /// Цена продажи
        /// </summary>
        public double Sell { get; set; }
        /// <summary>
        /// Цена покупки
        /// </summary>
        public double Buy { get; set; }
    }
}
