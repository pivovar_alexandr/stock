﻿using StockDomainAbstractions.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StockEntities.Code
{
    [Table("Divisions")]
    public class Division : DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        /// <summary>
        /// Название отдела
        /// </summary>
        [StringLength(64)]
        public string Name { get; set; }
        /// <summary>
        /// Сотрудник хранит Id отдела
        /// /// Отношение один ко многим
        /// </summary>
        public virtual List<Employee> Employees { get; set; }
        /// <summary>
        /// Продукт хранит Id отдела
        /// /// Отношение один ко многим
        /// </summary>
        public virtual List<Product> Products { get; set; }
        /// <summary>
        /// Отношение многие ко многим
        /// </summary>
        public virtual List<Category> Categories { get; set; }
    }
}
