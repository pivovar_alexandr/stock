﻿using StockDomainAbstractions.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StockEntities.Code
{
    [Table("LogPasss")]
    public class LogPass : DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        /// <summary>
        /// Логин работника
        /// </summary>
        [StringLength(16)]
        public string Login { get; set; }
        /// <summary>
        /// Пароль работника
        /// </summary>
        [StringLength(64)]
        public string Password { get; set; }
    }
}
