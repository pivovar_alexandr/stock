﻿using StockDomainAbstractions.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StockEntities.Code
{
    [Table("Products")]
    public class Product : DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        /// <summary>
        /// Название продукта
        /// </summary>
        [StringLength(32)]
        public string Name { get; set; }
        /// <summary>
        /// Описание товара
        /// Отношение один к одному
        /// </summary>
        public virtual Discription Discriptions { get; set; }
        /// <summary>
        /// Хранит Id отдела
        /// Отношение многие к одному
        /// </summary>
        public virtual Division Divisions { get; set; }
        /// <summary>
        /// Храните Id категорий
        /// Отношение многие к одному
        /// </summary>
        public virtual Category Categorys { get; set; }
        /// <summary>
        /// Отношение многие ко многим
        /// </summary>
        public virtual List<InvoiceIn> InvoiceIn { get; set; }
        /// <summary>
        /// Отношение многие ко многим
        /// </summary>
        public virtual List<InvoiceOut> InvoiceOut { get; set; }
    }
}
