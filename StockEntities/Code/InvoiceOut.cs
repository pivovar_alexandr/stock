﻿using StockDomainAbstractions.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StockEntities.Code
{
    [Table("InvoiceOut")]
    public class InvoiceOut : DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        /// <summary>
        /// Дата оформления накладной
        /// </summary>
        public DateTime Data { get; set; }
        /// <summary>
        /// Многие к одному
        /// Расходная накладная хранит Id отдела
        /// </summary>
        public virtual List<Product> Products { get; set; }
        /// <summary>
        /// Хранит Id сотрудника
        /// Отношение один ко многим
        /// </summary>
        public virtual Employee Employees { get; set; }
    }
}
