﻿using StocksAppContext.Code;
using StockRepositories;
using StockRepositories.Abstract;

namespace StockDataUnit
{
    public static class Unit
    {
        static MyAppDbContext _context;
        public static IProductsRepository ProductsRepository { get; private set; }
        public static IDiscriptionsRepository DiscriptionsRepository { get; private set; }
        public static IDivisionsRepository DivisionsRepository { get; private set; }
        public static IEmployeesRepository EmployeesRepository { get; private set; }
        public static IInvoiceInRepository InvoiceInRepository { get; private set; }
        public static IInvoiceOutRepository InvoiceOutRepository { get; private set; }
        public static ILogPasssRepository LogPasssRepository { get; private set; }
        public static IEmployeeProfileRepository EmployeeProfileRepository { get; private set; }
        
        static Unit()
        {
            _context = new MyAppDbContext("Stock");
            ProductsRepository = new ProductsRepository(_context);
            DiscriptionsRepository = new DiscriptionsRepository(_context);
            DivisionsRepository = new DivisionsRepository(_context);
            EmployeesRepository = new EmployeesRepository(_context);
            InvoiceInRepository = new InvoiceInRepository(_context);
            InvoiceOutRepository = new InvoiceOutRepository(_context);
            LogPasssRepository = new LogPasssRepository(_context);
            EmployeeProfileRepository = new EmployeeProfileRepository(_context);
        }
    }
}