﻿using Stock.BLL.DTO;
using System.Collections.Generic;
using System.Linq;
using StockDataUnit;
using StockEntities.Code;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Stock.BLL.Services;

namespace Stock.BLL.Test.Classes.ServiceTests
{
    [TestClass]
    public class EmployeeServiceTest
    {
        [TestMethod]
        public void ChangeItem()
        {
            EmployeeService es = new EmployeeService();
            EmployeeDTO em = new EmployeeDTO
            {
                Name = "test",
                Divisions = new DivisionDTO { },
                LogPasss = new LogPassDTO { },
                EmployeeProfiles = new EmployeeProfileDTO { }
            };
            es.AddItem(em);
            em.Name = "testNew";
            es.ChangeItem(em);
        }
        [TestMethod]
        public void AddItemsTest()
        {
            foreach (var item in Unit.EmployeesRepository.AllItems.ToArray())
            {
                Unit.EmployeesRepository.DeleteItem(item.Id);
            }
            EmployeeService es = new EmployeeService();
            List<EmployeeDTO> list = new List<EmployeeDTO>();
            EmployeeDTO e = new EmployeeDTO
            {
                Name = "12",
                LogPasss = new LogPassDTO { },
                EmployeeProfiles = new EmployeeProfileDTO { },
                Divisions = new DivisionDTO { }
            };
            for (int i = 0; i < 3; i++)
            {
                e.Name = i.ToString();
                list.Add(e);
            }
            int count = Unit.EmployeesRepository.AllItems.Count();
            es.AddItems(list);
            Assert.AreEqual(count + 3, Unit.EmployeesRepository.AllItems.Count());
        }
        [TestMethod]
        public void AllItemsGetTest()
        {
            EmployeeService es = new EmployeeService();
            IEnumerable<EmployeeDTO> e = es.AllItems;
            Assert.AreNotEqual(null, e);
        }
        [TestMethod]
        public void GetItemTest()
        {
            EmployeeService es = new EmployeeService();
            Employee addEmp = new Employee
            {
                Name = "Ivan",
                Divisions = new Division(),
                EmployeeProfiles = new EmployeeProfile(),
                LogPasss = new LogPass()
            };
            Unit.EmployeesRepository.AddItem(addEmp);
            EmployeeDTO emp = es.GetItem(addEmp.Id);
            Assert.AreEqual("Ivan", emp.Name);
        }
        [TestMethod]
        public void AddItemTest()
        {
            EmployeeService es = new EmployeeService();
            EmployeeDTO empDTO = new EmployeeDTO
            {
                Name = "Ivan ivanich",
                Divisions = new DivisionDTO(),
                EmployeeProfiles = new EmployeeProfileDTO(),
                LogPasss = new LogPassDTO()
            };
            Assert.AreEqual(true, es.AddItem(empDTO));
            Employee employee = Unit.EmployeesRepository.GetItem(empDTO.Id);

            Employee r = Unit.EmployeesRepository.AllItems.FirstOrDefault(
                x => x.Name.CompareTo("Ivan ivanich") == 0);
            Assert.AreNotEqual(null, r);
        }
        [TestMethod]
        public void AddNotEmptyProperty()
        {
            EmployeeService es = new EmployeeService();
            EmployeeDTO empDTO1 = new EmployeeDTO { Name = "", Divisions = null, EmployeeProfiles = null };
            EmployeeDTO empDTO2 = new EmployeeDTO { Name = "", Divisions = null };
            EmployeeDTO empDTO3 = new EmployeeDTO { Name = "", EmployeeProfiles = null, LogPasss = null };
            Assert.AreEqual(false, es.AddItem(empDTO1));
            Assert.AreEqual(false, es.AddItem(empDTO2));
            Assert.AreEqual(false, es.AddItem(empDTO3));
        }
        [TestMethod]
        public void DeleteItemTest()
        {
            Assert.AreNotEqual(0, Unit.EmployeesRepository.AllItems.Count());
            EmployeeService es = new EmployeeService();
            foreach (var item in Unit.EmployeesRepository.AllItems.ToList())
            {
                es.DeleteItem(item.Id);
            }
            Assert.AreEqual(0, Unit.EmployeesRepository.AllItems.Count());
        }
        [TestMethod]
        public void DeleteItemTestBool()
        {
            EmployeeService es = new EmployeeService();
            Employee em = new Employee();
            Unit.EmployeesRepository.AddItem(em);
            Assert.AreEqual(true, es.DeleteItem(em.Id));
        }
    }
}
