﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Stock.BLL.Services;
using StockDataUnit;
using System.Collections.Generic;
using System.Linq;
using StockEntities.Code;
using Stock.BLL.DTO;

namespace Stock.BLL.Test.Classes.ServiceTests
{
    [TestClass]
    public class LogPassServiceTest
    {
        [TestMethod]
        public void AddItemTest()
        {
            LogPassService ls = new LogPassService();
            LogPassDTO lpDTO = new LogPassDTO
            {
                Login = "asd",
                Password = "123"
            };

            ls.AddItem(lpDTO);

            LogPass lp = Unit.LogPasssRepository.GetItem(lpDTO.Id);

            if (lp != null)
            {
                Assert.AreEqual("asd", lp.Login);
            }
        }
        [TestMethod]
        public void AddItemNullCurruntTest()
        {
            LogPassService lps = new LogPassService();
            lps.AddItem(new LogPassDTO { Login = "", Password = "" });
            lps.AddItem(new LogPassDTO { Login = "12", Password = "" });
            lps.AddItem(new LogPassDTO { Login = "", Password = "321" });
            foreach (var item in Unit.LogPasssRepository.AllItems.ToList())
            {
                if (item.Login.CompareTo("") == 0 || item.Password.CompareTo("") == 0)
                {
                    Assert.Fail();
                }
            }
        }
        [TestMethod]
        public void DeleteItemTest()
        {
            Unit.LogPasssRepository.AddItem(new LogPass { Login = "TestDelete", Password = "12" });
            LogPass lg = Unit.LogPasssRepository.AllItems.FirstOrDefault(
                x => x.Login.CompareTo("TestDelete") == 0);
            LogPassDTO lgdto = new LogPassDTO { Id = lg.Id, Login = lg.Login, Password = lg.Password };
            LogPassService ls = new LogPassService();
            if (ls.DeleteItem(lgdto.Id))
            {
                LogPass test = Unit.LogPasssRepository.AllItems.FirstOrDefault(
                    x => x.Login.CompareTo("TestDelete") == 0);
                Assert.IsNull(test);
            }
            else Assert.Fail();
        }
        [TestMethod]
        public void ChangeItemTest()
        {
            LogPassService lps = new LogPassService();
            foreach (var item in Unit.LogPasssRepository.AllItems.ToList())
            {
                Unit.LogPasssRepository.DeleteItem(item.Id);
            }
            LogPass l = new LogPass { Login = "Change", Password = "123" };
            Unit.LogPasssRepository.AddItem(l);

            lps.ChangeItem(new LogPassDTO { Id = l.Id, Login = "New Change", Password = l.Password });
            // Проблемы сохраненения
            Assert.AreNotEqual("New Change", Unit.LogPasssRepository.GetItem(l.Id).Login);

        }
        [TestMethod]
        public void GetItemTest()
        {
            LogPassService ls = new LogPassService();
            Unit.LogPasssRepository.AddItem(new LogPass { Login = "Ivan", Password = "123" });
            LogPass lp = Unit.LogPasssRepository.AllItems.FirstOrDefault(
                x => x.Login.CompareTo("Ivan") == 0);
            if (lp != null)
            {
                Assert.AreEqual("Ivan", lp.Login);
            }
            else { Assert.Fail(); }

        }
        [TestMethod]
        public void AddItemsTest()
        {
            LogPassService lps = new LogPassService();
            List<LogPassDTO> list = new List<LogPassDTO>();
            foreach (var item in lps.AllItems.ToList())
            {
                lps.DeleteItem(item.Id);
            }
            //Unit.LogPasssRepository.SaveChanges();
            list.Add(new LogPassDTO { Login = "list", Password = "list" });
            list.Add(new LogPassDTO { Login = "list", Password = "list" });
            list.Add(new LogPassDTO { Login = "list", Password = "list" });
            lps.AddItems(list);
            Assert.AreEqual(3, lps.AllItems.Count());
        }
    }
}
