﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Stock.BLL.Services;
using Stock.BLL.DTO;
using StockDataUnit;
using System.Linq;
using StockEntities.Code;
using System;

namespace Stock.BLL.Test.Classes.ServiceTests
{
    [TestClass]
    public class DivisionTest
    {
        [TestMethod]
        public void AddItemTest()
        {
            DivisionService ds = new DivisionService();

            // Добавляем в базу
            ds.AddItem(new DivisionDTO { Name = "Тех отдел" });
            // Проверяем на наличие в базе
            Division division = Unit.DivisionsRepository.AllItems.FirstOrDefault(
                x => x.Name.CompareTo("Тех отдел") == 0);
            Assert.AreNotEqual(null, division);
        }
        [TestMethod]
        public void NotReplayItemsTest()
        {
            DivisionService ds = new DivisionService();

            bool res = ds.AddItem(new DivisionDTO { Name = "Тех отдел" });
            Assert.AreEqual(false, res);
        }
        [TestMethod]
        public void GetItemsTest()
        {
            DivisionService ds = new DivisionService();

            var li = Unit.DivisionsRepository.AllItems.ToList();

            var en = li.GetEnumerator();
            bool res = true;

            foreach (var item in ds.AllItems)
            {
                if (!en.MoveNext())
                {
                    break;
                }
                if (en.Current.Id != item.Id && en.Current.Name.CompareTo(item.Name) != 0)
                {
                    res = false;
                    break;
                }
            }

            Assert.AreEqual(true, res);
        }
        [TestMethod]
        public void GetItemsCountTest()
        {
            DivisionService ds = new DivisionService();
            List<DivisionDTO> list_1 = ds.AllItems.ToList();
            List<Division> list_2 = Unit.DivisionsRepository.AllItems.ToList();
            Assert.AreEqual(list_1.Count, list_2.Count);
        }
        [TestMethod]
        public void DeleteItemTest()
        {
            DivisionService ds = new DivisionService();
            ds.AddItem(new DivisionDTO { Name = "Продуктовый" });

            DivisionDTO d = ds.AllItems.FirstOrDefault(x => x.Name.CompareTo("Продуктовый") == 0);

            if (d != null)
            {
                Assert.AreEqual(true, ds.DeleteItem(d.Id));
            }
            Assert.AreNotEqual(null, d);
        }
        [TestMethod]
        public void ChangeItemTest()
        {
            DivisionService ds = new DivisionService();
            Guid g;
            if (ds.AllItems.Count() != 0)
            {
                IEnumerator<DivisionDTO> e = ds.AllItems.GetEnumerator();
                if (e.MoveNext())
                {
                    // Запоминаем ID
                    g = e.Current.Id;
                    // меняем имя отдела
                    e.Current.Name = "Test";
                    // вызываем функцию
                    if (ds.ChangeItem(e.Current))
                    {
                        Assert.AreEqual("Test", ds.GetItem(g).Name);
                    }
                }
                else
                {
                    Assert.Fail();
                }
            }
            else
            {
                Assert.Fail();
            }

        }
        [TestMethod]
        public void GetItemTest()
        {
            // Добавить елемент
            DivisionService ds = new DivisionService();
            ds.AddItem(new DivisionDTO { Name = "Ткани" });
            // Найти его id
            Division d = Unit.DivisionsRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("Ткани") == 0);
            // получить по id елемент
            Assert.AreNotEqual(null, d);
            if (d != null)
            {
                DivisionDTO division = ds.GetItem(d.Id);
                Assert.AreNotEqual(null, division);
                Assert.AreEqual("Ткани", division.Name);
            }
        }
        [TestMethod]
        public void AddItemsTest()
        {
            DivisionService ds = new DivisionService();
            List<DivisionDTO> divisionDTOs = new List<DivisionDTO>();
            foreach (var item in Unit.DivisionsRepository.AllItems.ToList())
            {
                Unit.DivisionsRepository.DeleteItem(item.Id);
            }

            divisionDTOs.Add(new DivisionDTO { Name = "1" });
            divisionDTOs.Add(new DivisionDTO { Name = "2" });
            divisionDTOs.Add(new DivisionDTO { Name = "3" });
            ds.AddItems(divisionDTOs);

            Assert.AreEqual(3, Unit.DivisionsRepository.AllItems.Count());
        }
    }
}
