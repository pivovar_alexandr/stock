﻿using System.Data.Entity;

namespace StocksAppContext.Code
{
    public class MyAppDbContextInitializer : DropCreateDatabaseIfModelChanges<MyAppDbContext>
    {
        protected override void Seed(MyAppDbContext context)
        {

        }
    }
}
