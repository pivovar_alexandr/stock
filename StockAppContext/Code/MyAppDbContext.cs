﻿using StockEntities.Code;
using System.Data.Entity;

namespace StocksAppContext.Code
{
    public class MyAppDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Discription> Discriptions { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<InvoiceIn> InvoiceIn { get; set; }
        public DbSet<InvoiceOut> InvoiceOut { get; set; }
        public DbSet<LogPass> LogPasss { get; set; }

        public MyAppDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }
        static MyAppDbContext()
        {
            Database.SetInitializer(new MyAppDbContextInitializer());
        }
    }
}
