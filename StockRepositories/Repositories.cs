﻿using StocksAppContext.Code;
using StockEntities.Code;
using StockRepositories.Generic;
using StockRepositories.Abstract;

namespace StockRepositories
{
    public class ProductsRepository : DbRepository<Product>, IProductsRepository
    {
        public ProductsRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }

    public class DivisionsRepository : DbRepository<Division>, IDivisionsRepository
    {
        public DivisionsRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }

    public class DiscriptionsRepository : DbRepository<Discription>, IDiscriptionsRepository
    {
        public DiscriptionsRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }
    public class EmployeesRepository : DbRepository<Employee>, IEmployeesRepository
    {
        public EmployeesRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }
    public class InvoiceInRepository : DbRepository<InvoiceIn>, IInvoiceInRepository
    {
        public InvoiceInRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }
    public class InvoiceOutRepository : DbRepository<InvoiceOut>, IInvoiceOutRepository
    {
        public InvoiceOutRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }
    public class LogPasssRepository : DbRepository<LogPass>, ILogPasssRepository
    {
        public LogPasssRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }
    public class EmployeeProfileRepository : DbRepository<EmployeeProfile>, IEmployeeProfileRepository
    {
        public EmployeeProfileRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }
}

