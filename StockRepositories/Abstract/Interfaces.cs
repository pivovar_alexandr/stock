﻿using StockDomainAbstractions.Repositories;
using StockEntities.Code;

namespace StockRepositories.Abstract
{
    public interface IProductsRepository : IDbRepository<Product>
    {
    }
    public interface IDiscriptionsRepository : IDbRepository<Discription>
    {
    }
    public interface IDivisionsRepository : IDbRepository<Division>
    {
    }
    public interface IEmployeesRepository : IDbRepository<Employee>
    {
    }
    public interface IInvoiceInRepository : IDbRepository<InvoiceIn>
    {
    }
    public interface IInvoiceOutRepository : IDbRepository<InvoiceOut>
    {
    }
    public interface ILogPasssRepository : IDbRepository<LogPass>
    {
    }
    public interface IEmployeeProfileRepository : IDbRepository<EmployeeProfile>
    {
    }
}
