﻿using System;
using System.Collections.Generic;

namespace Stock.BLL.Repositories
{
    public interface IDtoRepository<T> where T : class
    {
        bool AddItem(T item);
        bool AddItems(IEnumerable<T> items);
        IEnumerable<T> AllItems { get; }
        bool ChangeItem(T item);
        bool DeleteItem(Guid id);
        T GetItem(Guid id);
        bool SaveChanges();
    }
}
