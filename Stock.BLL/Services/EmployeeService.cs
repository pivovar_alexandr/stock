﻿using Stock.BLL.Abstract;
using Stock.BLL.DTO;
using StockDataUnit;
using StockEntities.Code;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Stock.BLL.Services
{
    public class EmployeeService : IEmployeeDTORepository
    {
        public IEnumerable<EmployeeDTO> AllItems
        {
            get
            {
                List<EmployeeDTO> list = new List<EmployeeDTO>();
                foreach (var item in Unit.EmployeesRepository.AllItems.ToList())
                {
                    list.Add((EmployeeDTO)item);
                }
                return list.AsEnumerable();
            }
        }
        public bool AddItem(EmployeeDTO item)
        {
            if (item.Id == null || item.Name == string.Empty
                || item.Name == null || item.EmployeeProfiles == null
                || item.Divisions == null)
                return false;
            
            Employee employee = (Employee)item;
            if (Unit.EmployeesRepository.AddItem(employee)) { return true; }
            else return false;
        }
        public bool AddItems(IEnumerable<EmployeeDTO> items)
        {
            List<Employee> em = new List<Employee>();
            foreach (var item in items.ToList())
            {
                em.Add(new Employee
                {
                    Id = item.Id,
                    Name = item.Name
                });
            }
            if (Unit.EmployeesRepository.AddItems(em))
                return true;
            else
                return false;
        }
        public bool ChangeItem(EmployeeDTO item)
        {
            Employee em = (Employee)item;
            if(Unit.EmployeesRepository.SaveChanges()) { return true; }

            return false;
        }
        public bool DeleteItem(Guid id)
        {
            if (Unit.EmployeesRepository.DeleteItem(id)) { return true; }
            else return false;
        }
        public EmployeeDTO GetItem(Guid id)
        {
            if (id == null) { throw new ArgumentNullException("Guid is null"); }
            return  (EmployeeDTO)Unit.EmployeesRepository.GetItem(id);
        }
        public bool SaveChanges()
        {
            if (Unit.EmployeesRepository.SaveChanges()) return true;
            else return false;
        }
    }
}
