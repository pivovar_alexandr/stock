﻿using Stock.BLL.Abstract;
using Stock.BLL.DTO;
using StockEntities.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockDataUnit;

namespace Stock.BLL.Services
{

    public class LogPassService : ILogPassDTORepository
    {
        public IEnumerable<LogPassDTO> AllItems
        {
            get
            {
                List<LogPassDTO> li = new List<LogPassDTO>(100);
                foreach (var item in Unit.LogPasssRepository.AllItems.ToList())
                {
                    li.Add(new LogPassDTO { Id = item.Id, Password = item.Password });
                }
                return li.AsEnumerable();
            }
        }
        public bool AddItem(LogPassDTO item)
        {
            if (item.Login == "" || item.Password == "") return false;

            if (item == null)
            {
                return false;
            }
            if (Unit.LogPasssRepository.AddItem((LogPass)item)) { return true; }
            return false;
        }

        public bool AddItems(IEnumerable<LogPassDTO> items)
        {
            if (items == null) return false;
            if (items.Count() == 0) return false;

            IEnumerator<LogPassDTO> e = items.GetEnumerator();
            List<LogPass> list = new List<LogPass>();
            while (e.MoveNext())
            {
                list.Add((LogPass)e.Current);
            }
            if (Unit.LogPasssRepository.AddItems(list)) return true;
            else return false;
        }

        public bool ChangeItem(LogPassDTO item)
        {
            Unit.LogPasssRepository.ChangeItem((LogPass)item);
            Unit.LogPasssRepository.SaveChanges();
            return true;
        }

        public bool DeleteItem(Guid id)
        {
            if (Unit.LogPasssRepository.DeleteItem(id)) { return true; }
            else return false;
        }

        public LogPassDTO GetItem(Guid id)
        {
            LogPassDTO result = (LogPassDTO)Unit.LogPasssRepository.GetItem(id);
            if (result == null) throw new NullReferenceException();
            else return result;
        }

        public bool SaveChanges()
        {
            if (Unit.LogPasssRepository.SaveChanges()) return true;
            else return false;
        }
    }
}
