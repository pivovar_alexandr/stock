﻿using Stock.BLL.Abstract;
using Stock.BLL.DTO;
using System;
using System.Collections.Generic;
using StockDataUnit;
using StockEntities.Code;
using System.Linq;

namespace Stock.BLL.Services
{
    public class DivisionService : IDivisionDTORepository
    {
        public IEnumerable<DivisionDTO> AllItems
        {
            get
            {
                List<DivisionDTO> li = new List<DivisionDTO>(100);
                foreach (var item in Unit.DivisionsRepository.AllItems.ToList())
                {
                    li.Add(new DivisionDTO { Id = item.Id, Name = item.Name });
                }
                return li.AsEnumerable();
            }
        }
        public bool AddItem(DivisionDTO item)
        {
            if (item.Name == "" || item.Name == string.Empty)
                return false;

            foreach (var it in Unit.DivisionsRepository.AllItems.ToList())
            {
                if (it.Name.CompareTo(item.Name) == 0)
                {
                    return false;
                }
            }

            Unit.DivisionsRepository.AddItem(new Division { Name = item.Name });
            return true;
        }
        public bool AddItems(IEnumerable<DivisionDTO> items)
        {
            if (items == null) { return false; }

            List<Division> divisionsList = new List<Division>();
            List<Division> re = Unit.DivisionsRepository.AllItems.ToList();

            foreach (var item in items)
            {// если хоть одно значение из списка есть в базе "return false"
                Division d = re.FirstOrDefault(x => x.Name.CompareTo(item.Name) == 0);
                if (d != null) { return false; }

                divisionsList.Add(new Division { Id = item.Id, Name = item.Name });
            }
            if (Unit.DivisionsRepository.AddItems(divisionsList)) {return true;}
            return false;
        }
        public bool ChangeItem(DivisionDTO item)
        {
            Division d = Unit.DivisionsRepository.GetItem(item.Id);
            if (d != null)
            {
                Division division = Unit.DivisionsRepository.AllItems.FirstOrDefault(
                    x => x.Name.CompareTo(item.Name) == 0);
                if (division != null)
                {
                    return false;
                }
                d.Name = item.Name;
                if (Unit.DivisionsRepository.SaveChanges()) { return true; }
            }
            return false;
        }
        public bool DeleteItem(Guid id)
        {
            if (Unit.DivisionsRepository.DeleteItem(id))
            {
                return true;
            }
            return false;

        }
        public DivisionDTO GetItem(Guid id)
        {
            Division division = Unit.DivisionsRepository.GetItem(id);
            if (division == null)
                return null;
            else
                return new DivisionDTO { Id = division.Id, Name = division.Name };
        }
        public bool SaveChanges()
        {
            if (Unit.DivisionsRepository.SaveChanges())
                return true;
            return false;
        }
    }
}
