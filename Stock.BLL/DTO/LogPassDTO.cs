﻿using StockEntities.Code;
using System;

namespace Stock.BLL.DTO
{
    public class LogPassDTO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Логин работника
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Пароль работника
        /// </summary>
        public string Password { get; set; }
        public static explicit operator LogPassDTO(LogPass value)
        {
            return new LogPassDTO
            {
                Id = value.Id,
                Login = value.Login,
                Password = value.Password
            };
        }
        public static explicit operator LogPass(LogPassDTO value)
        {
            return new LogPass
            {
                Id = value.Id,
                Login = value.Login,
                Password = value.Password
            };
        }
    }
}
