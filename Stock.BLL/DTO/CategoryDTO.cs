﻿using StockEntities.Code;
using System;

namespace Stock.BLL.DTO
{
    public class CategoryDTO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Название категории
        /// </summary>
        public string NameCategory { get; set; }
        public static explicit operator CategoryDTO(Category value)
        {
            return new CategoryDTO
            {
                Id = value.Id,
                NameCategory = value.NameCategory
            };
        }
        public static explicit operator Category(CategoryDTO value)
        {
            return new Category
            {
                Id = value.Id,
                NameCategory = value.NameCategory
            };
        }
    }
}
