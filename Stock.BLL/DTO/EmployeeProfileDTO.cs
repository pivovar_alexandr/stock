﻿using StockEntities.Code;
using System;

namespace Stock.BLL.DTO
{
    public class EmployeeProfileDTO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Возраст сотрудника
        /// </summary>
        public int Age { get; set; }
        /// <summary>
        /// Зарплата
        /// </summary>
        public double Salary { get; set; }
        /// <summary>
        /// Адрес прожвания
        /// </summary>
        public string Adress { get; set; }
        /// <summary>
        /// Номер телефона
        /// </summary>
        public string NumberPhone { get; set; }
        public static explicit operator EmployeeProfileDTO(EmployeeProfile value)
        {
            return new EmployeeProfileDTO
            {
                Adress = value.Adress,
                Age = value.Age,
                Id = value.Id,
                NumberPhone = value.NumberPhone,
                Salary = value.Salary
            };
        }
        public static explicit operator EmployeeProfile(EmployeeProfileDTO value)
        {
            return new EmployeeProfile
            {
                Adress = value.Adress,
                Age = value.Age,
                Id = value.Id,
                NumberPhone = value.NumberPhone,
                Salary = value.Salary
            };
        }
    }
}
