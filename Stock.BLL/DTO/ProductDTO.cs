﻿using StockEntities.Code;
using System;

namespace Stock.BLL.DTO
{
    public class ProductDTO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Название продукта
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Описание товара
        /// Отношение один к одному
        /// </summary>
        public DiscriptionDTO Discriptions { get; set; }
        /// <summary>
        /// Хранит Id отдела
        /// Отношение многие к одному
        /// </summary>
        public DivisionDTO Divisions { get; set; }
        /// <summary>
        /// Храните Id категорий
        /// Отношение многие к одному
        /// </summary>
        public CategoryDTO Categorys { get; set; }
        public static explicit operator ProductDTO(Product value)
        {
            return new ProductDTO
            {
                Categorys = (CategoryDTO)value.Categorys,
                Discriptions = (DiscriptionDTO)value.Discriptions,
                Divisions = (DivisionDTO)value.Divisions,
                Id = value.Id,
                Name = value.Name
            };
        }
        public static explicit operator Product(ProductDTO value)
        {
            return new Product
            {
                Categorys = (Category)value.Categorys,
                Discriptions = (Discription)value.Discriptions,
                Divisions = (Division)value.Divisions,
                Id = value.Id,
                Name = value.Name
            };
        }
    }
}
