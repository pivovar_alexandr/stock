﻿using StockEntities.Code;
using System;
using System.Collections.Generic;

namespace Stock.BLL.DTO
{
    public class InvoiceInDTO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Дата оформления накладной
        /// </summary>
        public DateTime Data { get; set; }
        /// <summary>
        /// Хранит Id сотрудника
        /// Отношение один ко многим
        /// </summary>
        public EmployeeDTO Employees { get; set; }
        public static implicit operator InvoiceInDTO(InvoiceIn value)
        {
            return new InvoiceInDTO
            {
                Data = value.Data,
                Employees = (EmployeeDTO)value.Employees,
                Id = value.Id
            };
        }
    }
}
