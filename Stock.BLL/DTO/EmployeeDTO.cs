﻿using StockEntities.Code;
using System;

namespace Stock.BLL.DTO
{
    public class EmployeeDTO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// ФИО
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Хранит Id Логин и пароль
        /// Отношение один к одному
        /// </summary>
        public LogPassDTO LogPasss { get; set; }
        /// <summary>
        /// Хранит Id отдела
        /// Отношение многие к одному
        /// </summary>
        public DivisionDTO Divisions { get; set; }
        /// <summary>
        /// Хранит Id профиля
        /// Отношение один к одному
        /// </summary>
        public EmployeeProfileDTO EmployeeProfiles { get; set; }
        public static explicit operator EmployeeDTO(Employee value)
        {
            return new EmployeeDTO
            {
                Id = value.Id,
                Name = value.Name,
                LogPasss = (LogPassDTO)value.LogPasss,
                Divisions = (DivisionDTO)value.Divisions,
                EmployeeProfiles = (EmployeeProfileDTO)value.EmployeeProfiles
            };
        }
        public static explicit operator Employee(EmployeeDTO value)
        {
            return new Employee
            {
                Id = value.Id,
                Name = value.Name,
                LogPasss = (LogPass)value.LogPasss,
                Divisions = (Division)value.Divisions,
                EmployeeProfiles = (EmployeeProfile)value.EmployeeProfiles
            };
        }
    }
}
