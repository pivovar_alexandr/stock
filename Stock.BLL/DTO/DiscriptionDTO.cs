﻿using StockEntities.Code;
using System;

namespace Stock.BLL.DTO
{
    public class DiscriptionDTO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Количество товара
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// Описание товара
        /// </summary>
        public string Dimension { get; set; }
        /// <summary>
        /// Цена продажи
        /// </summary>
        public double Sell { get; set; }
        /// <summary>
        /// Цена покупки
        /// </summary>
        public double Buy { get; set; }
        public static explicit operator DiscriptionDTO(Discription value)
        {
            return new DiscriptionDTO
            {
                Buy = value.Buy,
                Count = value.Count,
                Dimension = value.Dimension,
                Id = value.Id,
                Sell = value.Sell
            };
        }
        public static explicit operator Discription(DiscriptionDTO value)
        {
            return new Discription
            {
                Buy = value.Buy,
                Count = value.Count,
                Dimension = value.Dimension,
                Id = value.Id,
                Sell = value.Sell
            };
        }
    }
}
