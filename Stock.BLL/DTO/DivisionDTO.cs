﻿using StockEntities.Code;
using System;

namespace Stock.BLL.DTO
{
    public class DivisionDTO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Название отдела
        /// </summary>
        public string Name { get; set; }
        public static explicit operator DivisionDTO(Division value)
        {
            return new DivisionDTO
            {
                Id = value.Id,
                Name = value.Name
            };
        }
        public static explicit operator Division(DivisionDTO value)
        {
            return new Division
            {
                Id = value.Id,
                Name = value.Name
            };
        }
    }
}
