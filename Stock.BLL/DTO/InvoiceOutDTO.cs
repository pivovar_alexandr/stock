﻿using StockEntities.Code;
using System;
using System.Collections.Generic;

namespace Stock.BLL.DTO
{
    public class InvoiceOutDTO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Дата оформления накладной
        /// </summary>
        public DateTime Data { get; set; }
        /// <summary>
        /// Хранит Id сотрудника
        /// Отношение один ко многим
        /// </summary>
        public virtual EmployeeDTO Employees { get; set; }
        public static explicit operator InvoiceOutDTO(InvoiceOut value)
        {
            return new InvoiceOutDTO
            {
                Data = value.Data,
                Employees = (EmployeeDTO)value.Employees,
                Id = value.Id
            };
        }
        public static explicit operator InvoiceOut(InvoiceOutDTO value)
        {
            return new InvoiceOut
            {
                Data = value.Data,
                Employees = (Employee)value.Employees,
                Id = value.Id
            };
        }
    }
}
