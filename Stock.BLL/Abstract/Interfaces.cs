﻿using Stock.BLL.DTO;
using Stock.BLL.Repositories;

namespace Stock.BLL.Abstract
{
    public interface ICaterogyDTORepository : IDtoRepository<CategoryDTO>
    {
    }
    public interface IDiscriptionDTORepository : IDtoRepository<DiscriptionDTO>
    {
    }
    public interface IDivisionDTORepository : IDtoRepository<DivisionDTO>
    {
    }
    public interface IEmployeeDTORepository : IDtoRepository<EmployeeDTO>
    {
    }
    public interface IEmployeeProfileDTORepository : IDtoRepository<EmployeeProfileDTO>
    {
    }
    public interface IInvoiceInsDTORepository : IDtoRepository<InvoiceInDTO>
    {
    }
    public interface IInvoiceOutDTORepository : IDtoRepository<InvoiceOutDTO>
    {
    }
    public interface ILogPassDTORepository : IDtoRepository<LogPassDTO>
    {
    }
    public interface IProductsDTORepository : IDtoRepository<ProductDTO>
    {
    }
}
